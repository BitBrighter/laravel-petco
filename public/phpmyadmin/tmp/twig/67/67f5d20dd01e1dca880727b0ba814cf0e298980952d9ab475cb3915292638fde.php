<?php

/* table/structure/display_partitions.twig */
class __TwigTemplate_1275c3bef3e27f37174981a033f5e1a0aa74267bdce16fe22c6c350d2447e82e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"partitions\">
    <fieldset>
        <legend>";
        // line 4
        echo _gettext("Partitions");
        // line 5
        echo PhpMyAdmin\Util::showMySQLDocu("partitioning");
        echo "
        </legend>";
        // line 7
        if (twig_test_empty(($context["partitions"] ?? null))) {
            // line 8
            echo call_user_func_array($this->env->getFunction('Message_notice')->getCallable(), array(_gettext("No partitioning defined!")));
        } else {
            // line 10
            echo "            <p>";
            // line 11
            echo _gettext("Partitioned by:");
            // line 12
            echo "                <code>";
            echo twig_escape_filter($this->env, ($context["partition_method"] ?? null), "html", null, true);
            echo "(";
            echo twig_escape_filter($this->env, ($context["partition_expression"] ?? null), "html", null, true);
            echo ")</code>
            </p>";
            // line 14
            if (($context["has_sub_partitions"] ?? null)) {
                // line 15
                echo "                <p>";
                // line 16
                echo _gettext("Sub partitioned by:");
                // line 17
                echo "                    <code>";
                echo twig_escape_filter($this->env, ($context["sub_partition_method"] ?? null), "html", null, true);
                echo "(";
                echo twig_escape_filter($this->env, ($context["sub_partition_expression"] ?? null), "html", null, true);
                echo ")</code>
                <p>";
            }
            // line 20
            echo "            <table>
                <thead>
                    <tr>
                        <th colspan=\"2\">#</th>
                        <th>";
            // line 24
            echo _gettext("Partition");
            echo "</th>";
            // line 25
            if (($context["has_description"] ?? null)) {
                // line 26
                echo "                            <th>";
                echo _gettext("Expression");
                echo "</th>";
            }
            // line 28
            echo "                        <th>";
            echo _gettext("Rows");
            echo "</th>
                        <th>";
            // line 29
            echo _gettext("Data length");
            echo "</th>
                        <th>";
            // line 30
            echo _gettext("Index length");
            echo "</th>
                        <th>";
            // line 31
            echo _gettext("Comment");
            echo "</th>
                        <th colspan=\"";
            // line 32
            echo ((($context["range_or_list"] ?? null)) ? ("7") : ("6"));
            echo "\">";
            // line 33
            echo _gettext("Action");
            // line 34
            echo "                        </th>
                    </tr>
                </thead>
                <tbody>";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["partitions"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["partition"]) {
                // line 39
                echo "                    <tr class=\"noclick";
                echo ((($context["has_sub_partitions"] ?? null)) ? (" marked") : (""));
                echo "\">";
                // line 40
                if (($context["has_sub_partitions"] ?? null)) {
                    // line 41
                    echo "                            <td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getOrdinal", array(), "method"), "html", null, true);
                    echo "</td>
                            <td></td>";
                } else {
                    // line 44
                    echo "                            <td colspan=\"2\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getOrdinal", array(), "method"), "html", null, true);
                    echo "</td>";
                }
                // line 46
                echo "                        <th>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getName", array(), "method"), "html", null, true);
                echo "</th>";
                // line 47
                if (($context["has_description"] ?? null)) {
                    // line 48
                    echo "                            <td>
                                <code>";
                    // line 50
                    echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getExpression", array(), "method"), "html", null, true);
                    // line 51
                    echo ((($this->getAttribute($context["partition"], "getMethod", array(), "method") == "LIST")) ? (" IN (") : (" < "));
                    // line 52
                    echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getDescription", array(), "method"), "html", null, true);
                    // line 53
                    echo ((($this->getAttribute($context["partition"], "getMethod", array(), "method") == "LIST")) ? (")") : (""));
                    // line 54
                    echo "</code>
                            </td>";
                }
                // line 57
                echo "                        <td class=\"value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getRows", array(), "method"), "html", null, true);
                echo "</td>
                        <td class=\"value\">";
                // line 59
                $context["data_length"] = PhpMyAdmin\Util::formatByteDown($this->getAttribute(                // line 60
$context["partition"], "getDataLength", array(), "method"), 3, 1);
                // line 64
                echo "                            <span>";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["data_length"] ?? null), 0, array(), "array"), "html", null, true);
                echo "</span>
                            <span class=\"unit\">";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute(($context["data_length"] ?? null), 1, array(), "array"), "html", null, true);
                echo "</span>
                        </td>
                        <td class=\"value\">";
                // line 68
                $context["index_length"] = PhpMyAdmin\Util::formatByteDown($this->getAttribute(                // line 69
$context["partition"], "getIndexLength", array(), "method"), 3, 1);
                // line 73
                echo "                            <span>";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["index_length"] ?? null), 0, array(), "array"), "html", null, true);
                echo "</span>
                            <span class=\"unit\">";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute(($context["index_length"] ?? null), 1, array(), "array"), "html", null, true);
                echo "</span>
                        </td>
                        <td>";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["partition"], "getComment", array(), "method"), "html", null, true);
                echo "</td>";
                // line 77
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["action_icons"] ?? null));
                foreach ($context['_seq'] as $context["action"] => $context["icon"]) {
                    // line 78
                    echo "                            <td>
                                <a href=\"tbl_structure.php\" data-post=\"";
                    // line 79
                    echo twig_escape_filter($this->env, ($context["url_query"] ?? null), "html", null, true);
                    // line 80
                    echo "&amp;partition_maintenance=1&amp;sql_query=";
                    // line 81
                    echo twig_escape_filter($this->env, twig_urlencode_filter(((((("ALTER TABLE " . PhpMyAdmin\Util::backquote(($context["table"] ?? null))) . " ") . $context["action"]) . " PARTITION ") . $this->getAttribute(                    // line 82
$context["partition"], "getName", array(), "method"))), "html", null, true);
                    echo "\"
                                    id=\"partition_action_";
                    // line 83
                    echo twig_escape_filter($this->env, $context["action"], "html", null, true);
                    echo "\"
                                    name=\"partition_action_";
                    // line 84
                    echo twig_escape_filter($this->env, $context["action"], "html", null, true);
                    echo "\"
                                    class=\"ajax\">";
                    // line 86
                    echo $context["icon"];
                    echo "
                                </a>
                            </td>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['action'], $context['icon'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 91
                if (($context["has_sub_partitions"] ?? null)) {
                    // line 92
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["partition"], "getSubPartitions", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["sub_partition"]) {
                        // line 93
                        echo "                                <tr class=\"noclick\">
                                    <td></td>
                                    <td>";
                        // line 95
                        echo twig_escape_filter($this->env, $this->getAttribute($context["sub_partition"], "getOrdinal", array(), "method"), "html", null, true);
                        echo "</td>
                                    <td>";
                        // line 96
                        echo twig_escape_filter($this->env, $this->getAttribute($context["sub_partition"], "getName", array(), "method"), "html", null, true);
                        echo "</td>";
                        // line 97
                        if (($context["has_description"] ?? null)) {
                            // line 98
                            echo "                                        <td></td>";
                        }
                        // line 100
                        echo "                                    <td class=\"value\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["sub_partition"], "getRows", array(), "method"), "html", null, true);
                        echo "</td>
                                    <td class=\"value\">";
                        // line 102
                        $context["data_length"] = PhpMyAdmin\Util::formatByteDown($this->getAttribute(                        // line 103
$context["sub_partition"], "getDataLength", array(), "method"), 3, 1);
                        // line 107
                        echo "                                        <span>";
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["data_length"] ?? null), 0, array(), "array"), "html", null, true);
                        echo "</span>
                                        <span class=\"unit\">";
                        // line 108
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["data_length"] ?? null), 1, array(), "array"), "html", null, true);
                        echo "</span>
                                    </td>
                                    <td class=\"value\">";
                        // line 111
                        $context["index_length"] = PhpMyAdmin\Util::formatByteDown($this->getAttribute(                        // line 112
$context["sub_partition"], "getIndexLength", array(), "method"), 3, 1);
                        // line 116
                        echo "                                        <span>";
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["index_length"] ?? null), 0, array(), "array"), "html", null, true);
                        echo "</span>
                                        <span class=\"unit\">";
                        // line 117
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["index_length"] ?? null), 1, array(), "array"), "html", null, true);
                        echo "</span>
                                    </td>
                                    <td>";
                        // line 119
                        echo twig_escape_filter($this->env, $this->getAttribute($context["sub_partition"], "getComment", array(), "method"), "html", null, true);
                        echo "</td>
                                    <td colspan=\"";
                        // line 120
                        echo ((($context["range_or_list"] ?? null)) ? ("7") : ("6"));
                        echo "\"></td>
                                </tr>";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_partition'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                }
                // line 124
                echo "                    </tr>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['partition'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 126
            echo "                </tbody>
            </table>";
        }
        // line 129
        echo "    </fieldset>
    <fieldset class=\"tblFooters print_ignore\">
        <form action=\"tbl_structure.php\" method=\"post\">";
        // line 132
        echo PhpMyAdmin\Url::getHiddenInputs(($context["db"] ?? null), ($context["table"] ?? null));
        echo "
            <input type=\"hidden\" name=\"edit_partitioning\" value=\"true\" />";
        // line 134
        if (twig_test_empty(($context["partitions"] ?? null))) {
            // line 135
            echo "                <input type=\"submit\" name=\"edit_partitioning\" value=\"";
            echo _gettext("Partition table");
            echo "\" />";
        } else {
            // line 137
            echo PhpMyAdmin\Util::linkOrButton(($context["remove_url"] ?? null), _gettext("Remove partitioning"), array("class" => "button ajax", "id" => "remove_partitioning"));
            // line 140
            echo "
                <input type=\"submit\" name=\"edit_partitioning\" value=\"";
            // line 141
            echo _gettext("Edit partitioning");
            echo "\" />";
        }
        // line 143
        echo "        </form>
    </fieldset>
</div>
";
    }

    public function getTemplateName()
    {
        return "table/structure/display_partitions.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  311 => 143,  307 => 141,  304 => 140,  302 => 137,  297 => 135,  295 => 134,  291 => 132,  287 => 129,  283 => 126,  277 => 124,  268 => 120,  264 => 119,  259 => 117,  254 => 116,  252 => 112,  251 => 111,  246 => 108,  241 => 107,  239 => 103,  238 => 102,  233 => 100,  230 => 98,  228 => 97,  225 => 96,  221 => 95,  217 => 93,  213 => 92,  211 => 91,  202 => 86,  198 => 84,  194 => 83,  190 => 82,  189 => 81,  187 => 80,  185 => 79,  182 => 78,  178 => 77,  175 => 76,  170 => 74,  165 => 73,  163 => 69,  162 => 68,  157 => 65,  152 => 64,  150 => 60,  149 => 59,  144 => 57,  140 => 54,  138 => 53,  136 => 52,  134 => 51,  132 => 50,  129 => 48,  127 => 47,  123 => 46,  118 => 44,  112 => 41,  110 => 40,  106 => 39,  102 => 38,  97 => 34,  95 => 33,  92 => 32,  88 => 31,  84 => 30,  80 => 29,  75 => 28,  70 => 26,  68 => 25,  65 => 24,  59 => 20,  51 => 17,  49 => 16,  47 => 15,  45 => 14,  38 => 12,  36 => 11,  34 => 10,  31 => 8,  29 => 7,  25 => 5,  23 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "table/structure/display_partitions.twig", "/home/vagrant/code/lufe/wordpress/phpmyadmin/templates/table/structure/display_partitions.twig");
    }
}
