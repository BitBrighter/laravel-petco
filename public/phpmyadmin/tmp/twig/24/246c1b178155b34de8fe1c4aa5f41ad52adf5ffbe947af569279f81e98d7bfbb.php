<?php

/* columns_definitions/column_attributes.twig */
class __TwigTemplate_f7d0193127c9dd084e7b54b6361a52762d20b8bc8706390fa1a1425fcdfa60de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["ci"] = 0;
        // line 6
        $context["ci_offset"] =  -1;
        // line 7
        echo "
<td class=\"center\">";
        // line 10
        $this->loadTemplate("columns_definitions/column_name.twig", "columns_definitions/column_attributes.twig", 10)->display(array("column_number" =>         // line 11
($context["column_number"] ?? null), "ci" =>         // line 12
($context["ci"] ?? null), "ci_offset" =>         // line 13
($context["ci_offset"] ?? null), "column_meta" =>         // line 14
($context["column_meta"] ?? null), "cfg_relation" =>         // line 15
($context["cfg_relation"] ?? null), "max_rows" =>         // line 16
($context["max_rows"] ?? null)));
        // line 18
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 19
        echo "</td>
<td class=\"center\">";
        // line 22
        $this->loadTemplate("columns_definitions/column_type.twig", "columns_definitions/column_attributes.twig", 22)->display(array("column_number" =>         // line 23
($context["column_number"] ?? null), "ci" =>         // line 24
($context["ci"] ?? null), "ci_offset" =>         // line 25
($context["ci_offset"] ?? null), "column_meta" =>         // line 26
($context["column_meta"] ?? null), "type_upper" =>         // line 27
($context["type_upper"] ?? null)));
        // line 29
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 30
        echo "</td>
<td class=\"center\">";
        // line 33
        $this->loadTemplate("columns_definitions/column_length.twig", "columns_definitions/column_attributes.twig", 33)->display(array("column_number" =>         // line 34
($context["column_number"] ?? null), "ci" =>         // line 35
($context["ci"] ?? null), "ci_offset" =>         // line 36
($context["ci_offset"] ?? null), "length_values_input_size" =>         // line 37
($context["length_values_input_size"] ?? null), "length_to_display" =>         // line 38
($context["length"] ?? null)));
        // line 40
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 41
        echo "</td>
<td class=\"center\">";
        // line 44
        $this->loadTemplate("columns_definitions/column_default.twig", "columns_definitions/column_attributes.twig", 44)->display(array("column_number" =>         // line 45
($context["column_number"] ?? null), "ci" =>         // line 46
($context["ci"] ?? null), "ci_offset" =>         // line 47
($context["ci_offset"] ?? null), "column_meta" =>         // line 48
($context["column_meta"] ?? null), "type_upper" =>         // line 49
($context["type_upper"] ?? null), "char_editing" =>         // line 50
($context["char_editing"] ?? null)));
        // line 52
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 53
        echo "</td>
<td class=\"center\">";
        // line 56
        echo PhpMyAdmin\Charsets::getCollationDropdownBox(        // line 57
($context["dbi"] ?? null),         // line 58
($context["disable_is"] ?? null), (("field_collation[" .         // line 59
($context["column_number"] ?? null)) . "]"), ((("field_" .         // line 60
($context["column_number"] ?? null)) . "_") . (($context["ci"] ?? null) - ($context["ci_offset"] ?? null))), (( !twig_test_empty($this->getAttribute(        // line 61
($context["column_meta"] ?? null), "Collation", array(), "array"))) ? ($this->getAttribute(($context["column_meta"] ?? null), "Collation", array(), "array")) : (null)), false);
        // line 63
        echo "
</td>
<td class=\"center\">";
        // line 67
        $this->loadTemplate("columns_definitions/column_attribute.twig", "columns_definitions/column_attributes.twig", 67)->display(array("column_number" =>         // line 68
($context["column_number"] ?? null), "ci" =>         // line 69
($context["ci"] ?? null), "ci_offset" =>         // line 70
($context["ci_offset"] ?? null), "column_meta" =>         // line 71
($context["column_meta"] ?? null), "extracted_columnspec" =>         // line 72
($context["extracted_columnspec"] ?? null), "submit_attribute" =>         // line 73
($context["submit_attribute"] ?? null), "attribute_types" =>         // line 74
($context["attribute_types"] ?? null)));
        // line 76
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 77
        echo "</td>
<td class=\"center\">";
        // line 80
        $this->loadTemplate("columns_definitions/column_null.twig", "columns_definitions/column_attributes.twig", 80)->display(array("column_number" =>         // line 81
($context["column_number"] ?? null), "ci" =>         // line 82
($context["ci"] ?? null), "ci_offset" =>         // line 83
($context["ci_offset"] ?? null), "column_meta" =>         // line 84
($context["column_meta"] ?? null)));
        // line 86
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 87
        echo "</td>";
        // line 88
        if ((array_key_exists("change_column", $context) &&  !twig_test_empty(($context["change_column"] ?? null)))) {
            // line 90
            echo "    <td class=\"center\">";
            // line 91
            $this->loadTemplate("columns_definitions/column_adjust_privileges.twig", "columns_definitions/column_attributes.twig", 91)->display(array("column_number" =>             // line 92
($context["column_number"] ?? null), "ci" =>             // line 93
($context["ci"] ?? null), "ci_offset" =>             // line 94
($context["ci_offset"] ?? null), "privs_available" =>             // line 95
($context["privs_available"] ?? null)));
            // line 97
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 98
            echo "    </td>";
        }
        // line 100
        if ( !($context["is_backup"] ?? null)) {
            // line 102
            echo "    <td class=\"center\">";
            // line 103
            $this->loadTemplate("columns_definitions/column_indexes.twig", "columns_definitions/column_attributes.twig", 103)->display(array("column_number" =>             // line 104
($context["column_number"] ?? null), "ci" =>             // line 105
($context["ci"] ?? null), "ci_offset" =>             // line 106
($context["ci_offset"] ?? null), "column_meta" =>             // line 107
($context["column_meta"] ?? null)));
            // line 109
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 110
            echo "    </td>";
        }
        // line 112
        echo "<td class=\"center\">";
        // line 114
        $this->loadTemplate("columns_definitions/column_auto_increment.twig", "columns_definitions/column_attributes.twig", 114)->display(array("column_number" =>         // line 115
($context["column_number"] ?? null), "ci" =>         // line 116
($context["ci"] ?? null), "ci_offset" =>         // line 117
($context["ci_offset"] ?? null), "column_meta" =>         // line 118
($context["column_meta"] ?? null)));
        // line 120
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 121
        echo "</td>
<td class=\"center\">";
        // line 124
        $this->loadTemplate("columns_definitions/column_comment.twig", "columns_definitions/column_attributes.twig", 124)->display(array("column_number" =>         // line 125
($context["column_number"] ?? null), "ci" =>         // line 126
($context["ci"] ?? null), "ci_offset" =>         // line 127
($context["ci_offset"] ?? null), "max_length" =>         // line 128
($context["max_length"] ?? null), "value" => (((($this->getAttribute(        // line 129
($context["column_meta"] ?? null), "Field", array(), "array", true, true) && twig_test_iterable(        // line 130
($context["comments_map"] ?? null))) && $this->getAttribute(        // line 131
($context["comments_map"] ?? null), $this->getAttribute(($context["column_meta"] ?? null), "Field", array(), "array"), array(), "array", true, true))) ? (twig_escape_filter($this->env, $this->getAttribute(        // line 132
($context["comments_map"] ?? null), $this->getAttribute(($context["column_meta"] ?? null), "Field", array(), "array"), array(), "array"))) : (""))));
        // line 134
        $context["ci"] = (($context["ci"] ?? null) + 1);
        // line 135
        echo "</td>";
        // line 137
        if (($context["is_virtual_columns_supported"] ?? null)) {
            // line 138
            echo "    <td class=\"center\">";
            // line 139
            $this->loadTemplate("columns_definitions/column_virtuality.twig", "columns_definitions/column_attributes.twig", 139)->display(array("column_number" =>             // line 140
($context["column_number"] ?? null), "ci" =>             // line 141
($context["ci"] ?? null), "ci_offset" =>             // line 142
($context["ci_offset"] ?? null), "column_meta" =>             // line 143
($context["column_meta"] ?? null), "char_editing" =>             // line 144
($context["char_editing"] ?? null), "expression" => (($this->getAttribute(            // line 145
($context["column_meta"] ?? null), "Expression", array(), "array", true, true)) ? ($this->getAttribute(($context["column_meta"] ?? null), "Expression", array(), "array")) : ("")), "options" =>             // line 146
($context["options"] ?? null)));
            // line 148
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 149
            echo "    </td>";
        }
        // line 152
        if (array_key_exists("fields_meta", $context)) {
            // line 153
            $context["current_index"] = 0;
            // line 154
            $context["cols"] = (twig_length_filter($this->env, ($context["move_columns"] ?? null)) - 1);
            // line 155
            $context["break"] = false;
            // line 156
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, ($context["cols"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["mi"]) {
                if ((($this->getAttribute($this->getAttribute(($context["move_columns"] ?? null), $context["mi"], array(), "array"), "name", array()) == $this->getAttribute(($context["column_meta"] ?? null), "Field", array(), "array")) &&  !($context["break"] ?? null))) {
                    // line 157
                    $context["current_index"] = $context["mi"];
                    // line 158
                    $context["break"] = true;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 160
            echo "
    <td class=\"center\">";
            // line 162
            $this->loadTemplate("columns_definitions/move_column.twig", "columns_definitions/column_attributes.twig", 162)->display(array("column_number" =>             // line 163
($context["column_number"] ?? null), "ci" =>             // line 164
($context["ci"] ?? null), "ci_offset" =>             // line 165
($context["ci_offset"] ?? null), "column_meta" =>             // line 166
($context["column_meta"] ?? null), "move_columns" =>             // line 167
($context["move_columns"] ?? null), "current_index" =>             // line 168
($context["current_index"] ?? null)));
            // line 170
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 171
            echo "    </td>";
        }
        // line 174
        if ((($this->getAttribute(($context["cfg_relation"] ?? null), "mimework", array(), "array") && ($context["browse_mime"] ?? null)) && $this->getAttribute(($context["cfg_relation"] ?? null), "commwork", array(), "array"))) {
            // line 175
            echo "    <td class=\"center\">";
            // line 177
            $this->loadTemplate("columns_definitions/mime_type.twig", "columns_definitions/column_attributes.twig", 177)->display(array("column_number" =>             // line 178
($context["column_number"] ?? null), "ci" =>             // line 179
($context["ci"] ?? null), "ci_offset" =>             // line 180
($context["ci_offset"] ?? null), "column_meta" =>             // line 181
($context["column_meta"] ?? null), "available_mime" =>             // line 182
($context["available_mime"] ?? null), "mime_map" =>             // line 183
($context["mime_map"] ?? null)));
            // line 185
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 186
            echo "    </td>
    <td class=\"center\">";
            // line 189
            $this->loadTemplate("columns_definitions/transformation.twig", "columns_definitions/column_attributes.twig", 189)->display(array("column_number" =>             // line 190
($context["column_number"] ?? null), "ci" =>             // line 191
($context["ci"] ?? null), "ci_offset" =>             // line 192
($context["ci_offset"] ?? null), "column_meta" =>             // line 193
($context["column_meta"] ?? null), "available_mime" =>             // line 194
($context["available_mime"] ?? null), "mime_map" =>             // line 195
($context["mime_map"] ?? null), "type" => "transformation"));
            // line 198
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 199
            echo "    </td>
    <td class=\"center\">";
            // line 202
            $this->loadTemplate("columns_definitions/transformation_option.twig", "columns_definitions/column_attributes.twig", 202)->display(array("column_number" =>             // line 203
($context["column_number"] ?? null), "ci" =>             // line 204
($context["ci"] ?? null), "ci_offset" =>             // line 205
($context["ci_offset"] ?? null), "column_meta" =>             // line 206
($context["column_meta"] ?? null), "mime_map" =>             // line 207
($context["mime_map"] ?? null), "type_prefix" => ""));
            // line 210
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 211
            echo "    </td>
    <td class=\"center\">";
            // line 214
            $this->loadTemplate("columns_definitions/transformation.twig", "columns_definitions/column_attributes.twig", 214)->display(array("column_number" =>             // line 215
($context["column_number"] ?? null), "ci" =>             // line 216
($context["ci"] ?? null), "ci_offset" =>             // line 217
($context["ci_offset"] ?? null), "column_meta" =>             // line 218
($context["column_meta"] ?? null), "available_mime" =>             // line 219
($context["available_mime"] ?? null), "mime_map" =>             // line 220
($context["mime_map"] ?? null), "type" => "input_transformation"));
            // line 223
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 224
            echo "    </td>
    <td class=\"center\">";
            // line 227
            $this->loadTemplate("columns_definitions/transformation_option.twig", "columns_definitions/column_attributes.twig", 227)->display(array("column_number" =>             // line 228
($context["column_number"] ?? null), "ci" =>             // line 229
($context["ci"] ?? null), "ci_offset" =>             // line 230
($context["ci_offset"] ?? null), "column_meta" =>             // line 231
($context["column_meta"] ?? null), "mime_map" =>             // line 232
($context["mime_map"] ?? null), "type_prefix" => "input_"));
            // line 235
            $context["ci"] = (($context["ci"] ?? null) + 1);
            // line 236
            echo "    </td>";
        }
    }

    public function getTemplateName()
    {
        return "columns_definitions/column_attributes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 236,  286 => 235,  284 => 232,  283 => 231,  282 => 230,  281 => 229,  280 => 228,  279 => 227,  276 => 224,  274 => 223,  272 => 220,  271 => 219,  270 => 218,  269 => 217,  268 => 216,  267 => 215,  266 => 214,  263 => 211,  261 => 210,  259 => 207,  258 => 206,  257 => 205,  256 => 204,  255 => 203,  254 => 202,  251 => 199,  249 => 198,  247 => 195,  246 => 194,  245 => 193,  244 => 192,  243 => 191,  242 => 190,  241 => 189,  238 => 186,  236 => 185,  234 => 183,  233 => 182,  232 => 181,  231 => 180,  230 => 179,  229 => 178,  228 => 177,  226 => 175,  224 => 174,  221 => 171,  219 => 170,  217 => 168,  216 => 167,  215 => 166,  214 => 165,  213 => 164,  212 => 163,  211 => 162,  208 => 160,  201 => 158,  199 => 157,  194 => 156,  192 => 155,  190 => 154,  188 => 153,  186 => 152,  183 => 149,  181 => 148,  179 => 146,  178 => 145,  177 => 144,  176 => 143,  175 => 142,  174 => 141,  173 => 140,  172 => 139,  170 => 138,  168 => 137,  166 => 135,  164 => 134,  162 => 132,  161 => 131,  160 => 130,  159 => 129,  158 => 128,  157 => 127,  156 => 126,  155 => 125,  154 => 124,  151 => 121,  149 => 120,  147 => 118,  146 => 117,  145 => 116,  144 => 115,  143 => 114,  141 => 112,  138 => 110,  136 => 109,  134 => 107,  133 => 106,  132 => 105,  131 => 104,  130 => 103,  128 => 102,  126 => 100,  123 => 98,  121 => 97,  119 => 95,  118 => 94,  117 => 93,  116 => 92,  115 => 91,  113 => 90,  111 => 88,  109 => 87,  107 => 86,  105 => 84,  104 => 83,  103 => 82,  102 => 81,  101 => 80,  98 => 77,  96 => 76,  94 => 74,  93 => 73,  92 => 72,  91 => 71,  90 => 70,  89 => 69,  88 => 68,  87 => 67,  83 => 63,  81 => 61,  80 => 60,  79 => 59,  78 => 58,  77 => 57,  76 => 56,  73 => 53,  71 => 52,  69 => 50,  68 => 49,  67 => 48,  66 => 47,  65 => 46,  64 => 45,  63 => 44,  60 => 41,  58 => 40,  56 => 38,  55 => 37,  54 => 36,  53 => 35,  52 => 34,  51 => 33,  48 => 30,  46 => 29,  44 => 27,  43 => 26,  42 => 25,  41 => 24,  40 => 23,  39 => 22,  36 => 19,  34 => 18,  32 => 16,  31 => 15,  30 => 14,  29 => 13,  28 => 12,  27 => 11,  26 => 10,  23 => 7,  21 => 6,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "columns_definitions/column_attributes.twig", "/home/vagrant/code/lufe/wordpress/phpmyadmin/templates/columns_definitions/column_attributes.twig");
    }
}
