<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserquizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userquizzes', function (Blueprint $table) {
            $table->id();
            $table->integer('quiz_id');
            $table->integer('quiz_position')->default(0);
            $table->string('quiz_progress')->default('incomplete');
            $table->integer('quiz_prize_id')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userquizzes');
    }
}
