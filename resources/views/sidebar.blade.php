<div class="col-md-2">
	<div id="sidebar" class="card">
		<div class="card-header">Main Menu</div>

		<div class="card-body">

			<ul class="menu">
				<li class="menuitem"><a href="{{ route('admin.quizzes') }}">Quizzes</a></li>
				<li class="menuitem"><a href="{{ route('admin.questions') }}">Questions</a></li>
				<li class="menuitem"><a href="{{ route('admin.prizes') }}">Prizes</a></li>
				<li class="menuitem"><a href="{{ route('admin.quiz.users') }}">Users</a></li>
				<li class="menuitem"><a href="{{ route('admin.profile') }}">Profile</a></li>
			</ul>

		</div>
	</div>
</div>
