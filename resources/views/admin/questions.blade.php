@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>QUIZ ITEMS</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.question.add') }}" class="btn btn-primary">+ New Question</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">

                    <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">Question</th>
                        <th scope="col">Answers</th>
                        <th scope="col">Correct Answers</th>
                        <th scope="col">Correct</th>
                        <th scope="col">InCorrect</th>
                        <th scope="col" width="160">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($questions as $question)
                    <tr>
                        <td>{{ $question->question }}</td>
                        <td>{{ $question->answers }}</td>
                        <td>{{ $question->correct_answer }}</td>
                        <td>{{ $question->response_correct }}</td>
                        <td>{{ $question->response_incorrect }}</td>
                        <td>
                            <a href="{{ route('admin.question.edit', $question->id) }}" class="btn btn-secondary">Edit</a>
							<a href='{{ route("admin.question.delete", $question->id) }}' class='btn btn-danger'>Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
