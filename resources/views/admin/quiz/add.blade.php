@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>QUIZ ITEMS</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.quizzes') }}" class="btn btn-primary">Back</a></div>
                    <div class="page-action"><a href="{{ route('admin.quiz.add') }}" class="btn btn-primary">+ New Quiz</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">
                    
                    <form action="{{ route('admin.quiz.save') }}" method="post">

                        @csrf

                        <div class="form-group">
                            <label for="q_name">Quiz Name <small>required</small></label>
                            <input type="text" class="form-control" name="q_name" id="q_name" required>
                        </div>

                        <div class="form-group">
                            <label for="q_thankyou">Thank You Message <small>required</small></label>
                            <textarea-counter fieldname="q_thankyou" fieldvalue="" fieldmax=1000 required="true"></textarea-counter>
                        </div>

                        <select-questions questions="{{ json_encode( $questions ) }}"></select-questions>

                        <button type="submit" class="btn btn-primary">Create Quiz</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
