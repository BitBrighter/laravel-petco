@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>QUIZZES</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.quiz.add') }}" class="btn btn-primary">+ New Quiz</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">

                    <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Thank You</th>
                        <th scope="col">Questions</th>
                        <th scope="col" width="160">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($quizzes as $quiz)
                    <tr>
                        <td>{{ $quiz->title }}</td>
                        <td>{{ $quiz->thankyou }}</td>
                        <td>
                            <a href="{{ route('admin.quiz.edit', $quiz->id) }}" class="btn btn-secondary">
                                <span class="badge badge-light">{{ count( explode(',', $quiz->questions) ) }}</span>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.quiz.edit', $quiz->id) }}" class="btn btn-secondary">Edit</a>
                            <a href='{{ route("admin.quiz.delete", $quiz->id) }}' class='btn btn-danger'>Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
