@extends('layouts.app')

@section('content')
<div class="container"> 

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>PRIZES</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.prize.add') }}" class="btn btn-primary">+ New Prize</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">

                    <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Sponsor</th>
                        <th scope="col">Status</th>
                        <th scope="col" width="160">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($prizes as $prize)
                    <tr>
                        <td>{{ $prize->title }}</td>
                        <td>{{ $prize->description }}</td>
                        <td>{{ $prize->sponsor }}</td>
                        <td>{!! ($prize->status)? "<span class='badge badge-pill badge-success'>Enabled</span>": "<span class='badge badge-pill badge-warning'>Disabled</span>" !!}</span></td>
                        <td>
                            <a href="{{ route('admin.prize.edit', $prize->id) }}" class="btn btn-secondary">Edit</a>
                            <a href="{{ route('admin.prize.delete', $prize->id) }}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
