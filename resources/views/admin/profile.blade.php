@extends('layouts.app')

@section('content')
<div class="container"> 

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>PROFILE</h1></div>
                <!--<div class="page-actions">
                    <div class="page-action"><a href="">+ Add Prize</a></div>
                </div>-->
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div id="editprofile" class="card editpanel">

                <div class="card-body">

                    <h5>Edit Profile</h5>

                    <form action="{{ route('admin.profile.update', $user->id) }}" method="post">

                        @csrf

                        <div class="form-group">
                            <label for="profile_name">Name <small>required</small></label>
                            <input type="text" class="form-control" name="profile_name" id="profile_name" value="{{ $user->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="profile_email">Email Address <small>required</small></label>
                            <input type="text" class="form-control" name="profile_email" id="profile_email" value="{{ $user->email }}" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>

                    </form>

                </div>
            </div>

            <div class="card editpanel">
                <div class="card-body">

                    <h5>Update Password</h5>

                    <form action="{{ route('admin.profile.update.password', $user->id) }}" method="post">

                        @csrf

                        <div class="form-group">
                            <label for="">Current Password <small>required</small></label>
                            <input type="text" class="form-control" name="password_current" id="password_current"required>
                        </div>
                        <div class="form-group">
                            <label for="password_new">New Password <small>required</small></label>
                            <input type="text" class="form-control" name="password_new" id="password_new" required>
                        </div>
                        <div class="form-group">
                            <label for="password_confirm">Confirm New Password <small>required</small></label>
                            <input type="text" class="form-control" name="password_confirm" id="password_confirm" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Change Password</button>

                    </form>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
