@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>QUIZ USERS</h1></div>
                <!--<div class="page-actions">
                    <div class="page-action"><a href="">+ Add Prize</a></div>
                </div>-->
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">
                    Quiz User list goes here...
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
