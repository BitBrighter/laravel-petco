@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>ADD PRIZE</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.prizes') }}" class="btn btn-primary">Back</a></div>
                    <div class="page-action"><a href="{{ route('admin.prize.add') }}" class="btn btn-primary">+ New Prize</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">

                <div class="card-body">
                    
                    <form action="{{ route('admin.prize.save') }}" method="post">

                        @csrf

                        <div class="form-group">
                            <label for="p_title">Prize Name <small>required</small></label>
                            <input type="text" class="form-control" name="p_title" id="p_title" required>
                        </div>
                        <div class="form-group">
                            <label for="p_description">Description <small>required</small></label>
                            <input type="text" class="form-control" name="p_description" id="p_description" required>
                        </div>
                        <div class="form-group">
                            <label for="p_sponsor">Sponsor <small>required</small></label>
                            <input type="text" class="form-control" name="p_sponsor" id="p_sponsor" required>
                        </div>
                        <div class="form-group">
                            <label for="p_status">Status</label>
							<select class="form-control" name="p_status" id="p_status">
								<option value="1">Enabled</option>
								<option value="0">Disabled</option>
							</select>
                        </div>

                        <button type="submit" class="btn btn-primary">Create Prize</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
