@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-headbar">
                <div class="page-title"><h1>QUIZ ITEMS</h1></div>
                <div class="page-actions">
                    <div class="page-action"><a href="{{ route('admin.questions') }}" class="btn btn-primary">Back</a></div>
                    <div class="page-action"><a href="{{ route('admin.question.add') }}" class="btn btn-primary">+ New Question</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">

        @include('sidebar')

        <div class="col-md-10">

            @include('messages')

            <div class="card">
                <!--<div class="card-header">Main Menu</div>-->

                <div class="card-body">
                    
                    <form action="{{ route('admin.question.save') }}" method="post">

                        @csrf

                        <div class="form-group">
                            <label for="qi_question">Question <small>required</small></label>
                            <textarea-counter fieldname="qi_question" fieldvalue="" fieldmax=1000 required="true"></textarea-counter>
                        </div>
                        <div class="form-group">
                            <label for="qi_answers">Answers <small>required</small></label>
                            <input type="text" class="form-control" name="qi_answers" id="qi_answers" required>
                        </div>
                        <div class="form-group">
                            <label for="qi_correct_answer">Correct Answer <small>required</small></label>
                            <input type="text" class="form-control" name="qi_correct_answer" id="qi_correct_answer" required>
                        </div>
                        <div class="form-group">
                            <label for="qi_response_correct">Response Correct</label>
                            <textarea-counter fieldname="qi_response_correct" fieldvalue="Correct" fieldmax=1000 ></textarea-counter>
                        </div>
                        <div class="form-group">
                            <label for="qi_response_incorrect">Response Incorrect</label>
                            <textarea-counter fieldname="qi_response_incorrect" fieldvalue="Incorrect" fieldmax=1000 ></textarea-counter>
                        </div>
                        <button type="submit" class="btn btn-primary">Create Question</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
