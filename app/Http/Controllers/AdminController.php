<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;
use App\Question;
use App\Quiz;
use App\Prize;
use App\User;
use Log;
use Auth;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin()
    {
        return view('admin');
    }


    /* Quizzes */
    public function adminQuizzes()
    {
        $quizzes = Quiz::all();
        return view('admin.quizzes', compact('quizzes'));
    }
    public function adminAddQuiz()
    {
        $questions = Question::all();
        return view('admin.quiz.add', compact('questions'));
    }
    public function adminSaveQuiz(Request $request)
    {

        $validatedData = $request->validate([
            'q_name' => 'required',
            'q_thankyou' => 'required|max:1000',
        ]);

        $quiz = new Quiz();
        $quiz->title = $request->input('q_name');
        $quiz->thankyou = $request->input('q_thankyou');

        if($request->has('q_items')):
        $quiz->questions = $request->input('q_items');
        endif;

        $quiz->save();

        return redirect('admin/quizzes')->with('success', 'Quiz created successfully.');
    }
    public function adminEditQuiz($id)
    {
        $quiz = Quiz::find($id);
        $questions = Question::all();
        return view('admin.quiz.edit', compact('quiz', 'questions'));
    }
    public function adminUpdateQuiz(Request $request, $id)
    {

        $validatedData = $request->validate([
            'q_name' => 'required',
            'q_thankyou' => 'required|max:1000',
        ]);

        $quiz = Quiz::find($id);
        $quiz->title = $request->input('q_name');
        $quiz->thankyou = $request->input('q_thankyou');

        if($request->has('q_items')):
        $quiz->questions = $request->input('q_items');
        endif;
        $quiz->save();

        return redirect('admin/quizzes')->with('success','Quiz Updated.');
    }
    public function adminDeleteQuiz($id)
    {
        $quiz = Quiz::find($id);
        $quiz->delete();
        return redirect('admin/quizzes')->with('success','Quiz Removed.');
    }


    /* Questions for quizzes */
    public function adminQuestions()
    {
        $questions = Question::all();
        return view('admin.questions', compact('questions'));
    }
    public function adminAddQuestion()
    {
        return view('admin.question.add');
    }
    public function adminSaveQuestion(Request $request)
    {

        $validatedData = $request->validate([
            'qi_question' => 'required|max:1000',
            'qi_answers' => 'required',
            'qi_correct_answer' => 'required',
            'qi_response_correct' => 'max:1000',
            'qi_response_incorrect' => 'max:1000',
        ]);


        $question = new Question();
        $question->question = $request->input('qi_question');
        $question->answers = $request->input('qi_answers');
        $question->correct_answer = $request->input('qi_correct_answer');
        $question->response_correct = ($request->has('qi_response_correct') && $request->input('qi_response_correct') != '')? $request->input('qi_response_correct'): 'Correct';
        $question->response_incorrect = ($request->has('qi_response_incorrect') && $request->input('qi_response_incorrect') != '')? $request->input('qi_response_incorrect'): 'Incorrect';
        $question->save();

        return redirect('admin/questions')->with('success', 'Quiz Item created successfully.');
    }
    public function adminEditQuestion($id)
    {
        $question = Question::find($id);
        return view('admin.question.edit', compact('question'));
    }
    public function adminUpdateQuestion(Request $request, $id)
    {

        $validatedData = $request->validate([
            'qi_question' => 'required|max:1000',
            'qi_answers' => 'required',
            'qi_correct_answer' => 'required',
            'qi_response_correct' => 'max:1000',
            'qi_response_incorrect' => 'max:1000',
        ]);

        $question = Question::find($id);
        $question->question = $request->input('qi_question');
        $question->answers = $request->input('qi_answers');
        $question->correct_answer = $request->input('qi_correct_answer');
        $question->response_correct = ($request->has('qi_response_correct') && $request->input('qi_response_correct') != '')? $request->input('qi_response_correct'): 'Correct';
        $question->response_incorrect = ($request->has('qi_response_incorrect') && $request->input('qi_response_incorrect') != '')? $request->input('qi_response_incorrect'): 'Incorrect';
        $question->save();

        return redirect('admin/question/edit/'.$id)->with('success','Question Updated.');
    }
    public function adminDeleteQuestion($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect('admin/questions')->with('success','Question Removed.');
    }


    /* Prizes for completed quizzes */
    public function adminPrizes()
    {
        $prizes = Prize::all();
        return view('admin.prizes', compact('prizes'));
    }
    public function adminAddPrize()
    {
        return view('admin.prize.add');
    }
    public function adminSavePrize(Request $request)
    {
        $validatedData = $request->validate([
            'p_title' => 'required',
            'p_description' => 'required',
            'p_sponsor' => 'required',
        ]);

        $prize = new Prize();
        $prize->title = $request->input('p_title');
        $prize->description = $request->input('p_description');
        $prize->sponsor = $request->input('p_sponsor');
        $prize->status = $request->input('p_status');
        $prize->save();

        return redirect('admin/prizes')->with('success', 'Prize created successfully.');
    }
    public function adminEditPrize($id)
    {
        $prize = Prize::find($id);
        return view('admin.prize.edit', compact('prize'));
    }
    public function adminUpdatePrize(Request $request, $id)
    {

        $prize = Prize::find($id);
        $prize->title = $request->input('p_title');
        $prize->description = $request->input('p_description');
        $prize->sponsor = $request->input('p_sponsor');
        $prize->status = $request->input('p_status');
        $prize->save();
        return redirect('admin/prize/edit/'.$id)->with('success','Prize Updated.');
    }
    public function adminDeletePrize($id)
    {
        $prize = Prize::find($id);
        $prize->delete();
        return redirect('admin/prizes')->with('success','Prize Removed.');
    }


    /* Update/Edit Profile */
    public function adminProfile()
    {
        $user = Auth::user();
        return view('admin.profile', compact('user'));
    }
    public function adminUpdateProfile(Request $request, $id)
    {
        $validatedData = $request->validate([
            'profile_name' => 'required',
            'profile_email' => 'required',
        ]);

        $user = User::find($id);
        $user->name = $request->input('profile_name');
        $user->email = $request->input('profile_email');
        $user->save();

        return redirect('admin/profile')->with('success','Profile Updated.');
    }
    public function adminUpdateProfilePassword(Request $request, $id)
    {
        $validatedData = $request->validate([
            'password_current' => ['required', 'password', 'min:8'],
            'password_new' => ['required', 'min:8'],
            'password_confirm' => ['same:password_new'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password_new)]);

        return redirect('admin/profile')->with('success','Password Updated.');
    }


    /* Users taking part in the quiz */
    public function adminQuizUsers(){
        return view('admin.quizusers');
    }


}
