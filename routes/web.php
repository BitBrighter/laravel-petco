<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified')->name('home');

/* admin endpoints or routes */

Route::get('/admin', 'AdminController@admin')->middleware('verified')->middleware('is_admin')->name('admin');

Route::get('/admin/quizzes', 'AdminController@adminQuizzes')->middleware('verified')->middleware('is_admin')->name('admin.quizzes');
Route::get('/admin/quiz/add', 'AdminController@adminAddQuiz')->middleware('verified')->middleware('is_admin')->name('admin.quiz.add');
Route::post('/admin/quiz/save', 'AdminController@adminSaveQuiz')->middleware('verified')->middleware('is_admin')->name('admin.quiz.save');
Route::get('/admin/quiz/edit/{id}', 'AdminController@adminEditQuiz')->middleware('verified')->middleware('is_admin')->name('admin.quiz.edit');
Route::post('/admin/quiz/update/{id}', 'AdminController@adminUpdateQuiz')->middleware('verified')->middleware('is_admin')->name('admin.quiz.update');
Route::get('/admin/quiz/delete/{id}', 'AdminController@adminDeleteQuiz')->middleware('verified')->middleware('is_admin')->name('admin.quiz.delete');

Route::get('/admin/questions', 'AdminController@adminQuestions')->middleware('verified')->middleware('is_admin')->name('admin.questions');
Route::get('/admin/question/add', 'AdminController@adminAddQuestion')->middleware('verified')->middleware('is_admin')->name('admin.question.add');
Route::post('/admin/question/save', 'AdminController@adminSaveQuestion')->middleware('verified')->middleware('is_admin')->name('admin.question.save');
Route::get('/admin/question/edit/{id}', 'AdminController@adminEditQuestion')->middleware('verified')->middleware('is_admin')->name('admin.question.edit');
Route::post('/admin/question/update/{id}', 'AdminController@adminUpdateQuestion')->middleware('verified')->middleware('is_admin')->name('admin.question.update');
Route::get('/admin/question/delete/{id}', 'AdminController@adminDeleteQuestion')->middleware('verified')->middleware('is_admin')->name('admin.question.delete');


Route::get('/admin/prizes', 'AdminController@adminPrizes')->middleware('verified')->middleware('is_admin')->name('admin.prizes');
Route::get('/admin/prize/add', 'AdminController@adminAddPrize')->middleware('verified')->middleware('is_admin')->name('admin.prize.add');
Route::post('/admin/prize/save', 'AdminController@adminSavePrize')->middleware('verified')->middleware('is_admin')->name('admin.prize.save');
Route::get('/admin/prize/edit/{id}', 'AdminController@adminEditPrize')->middleware('verified')->middleware('is_admin')->name('admin.prize.edit');
Route::post('/admin/prize/update/{id}', 'AdminController@adminUpdatePrize')->middleware('verified')->middleware('is_admin')->name('admin.prize.update');
Route::get('/admin/prize/delete/{id}', 'AdminController@adminDeletePrize')->middleware('verified')->middleware('is_admin')->name('admin.prize.delete');

Route::get('/admin/quizusers', 'AdminController@adminQuizUsers')->middleware('verified')->middleware('is_admin')->name('admin.quiz.users');

Route::get('/admin/profile', 'AdminController@adminProfile')->middleware('verified')->middleware('is_admin')->name('admin.profile');
Route::post('/admin/profile/update/{id}', 'AdminController@adminUpdateProfile')->middleware('verified')->middleware('is_admin')->name('admin.profile.update');
Route::post('/admin/profile/update/password/{id}', 'AdminController@adminUpdateProfilePassword')->middleware('verified')->middleware('is_admin')->name('admin.profile.update.password');
